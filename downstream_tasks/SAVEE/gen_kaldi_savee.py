wav_scp_template = "sox $filepath -t wav -r 16k -b 16 -e signed - |"

class2num4 = {'a':'0','d':'0', 'f':'1','sa':'1', 'h':'2', 'su':'2' , 'n':'3'}
class2num7 = {'a':'0','d':'1', 'f':'2','sa':'3', 'h':'4', 'su':'5' , 'n':'6'}

my_classes = []

# Dataset loader for SAVEE
#
# S.  Haq,  P.  J.  Jackson,  and  J.  Edge,  “Speaker-dependent  audio-visual emotion recognition.” in AVSP, 2009, pp. 53–58.
#
# This dataset was designed as a speaker dependent dataset (there are just 4 male speakers)
# There is no official split and the dataset is very small, with 120 utterances per speaker.
# In the original evaluation, the train/test split was random per utterance
# 
# However, it appears that in "Towards Learning a Universal Non-Semantic Representation of Speech", Joel Shor, Aren Jansen, Ronnie Maor, Oran Lang, Omry Tuval, Félix de Chaumont Quitry, Marco Tagliasacchi, Ira Shavitt, Dotan Emanuel, Yinnon Haviv
# The test speaker is 'DC' and the validation speaker is 'JE'. The remaining two speakers are the training set.
#
# While this information is not in the paper, its in the tensorflow dataset supplied in the tdfs pip package.
#
# import tensorflow_datasets as tfds
# savee = tfds.load('savee')
# list(savee['test'])
# list(savee['validation'])
#
#
# Results will vary greatly depending on the test speaker! So its important to follow the tensorflow split
#


with open("SAVEE_filelist.txt") as in_file, open('wav.scp','w') as wav_scp, open('text', 'w') as text, open('utt2spk', 'w') as utt2spk, open('spk2utt', 'w') as spk2utt, open('utt2class', 'w') as utt2class, open('utt2class4', 'w') as utt2class4, open('utt2class7','w') as utt2class7, open('utt2set','w') as utt2set:
    for line in in_file:
        if line[-1] == '\n':
            line = line[:-1]
        print(line)
        filename = line
        line_split = line.split('/')
        myid = line_split[-1].split('.')[0]
        myclass = myid[0]
        if myid[1].isalpha():
            myclass += myid[1]
        speaker = line_split[1]

        kaldiid = speaker + '_' + myid

        print(speaker, myid, line)

        wav_scp.write(kaldiid+' '+wav_scp_template.replace('$filepath',filename)+'\n')
        spk2utt.write(speaker+' '+kaldiid+'\n')
        utt2spk.write(kaldiid+' '+speaker+'\n')
        text.write(kaldiid+' dummy\n')
        utt2class.write(kaldiid+' '+myclass+'\n')
        print(myclass)
        utt2class4.write(kaldiid+' '+class2num4[myclass]+'\n')
        utt2class7.write(kaldiid+' '+class2num7[myclass]+'\n')

        if speaker == 'DC':
            utt2set.write(kaldiid+' '+'test'+'\n')
        elif speaker == 'JE':
            utt2set.write(kaldiid+' '+'dev'+'\n')
        else:
            utt2set.write(kaldiid+' '+'train'+'\n')

        my_classes.append(myclass)

print('Encountered the following classes:', list(set(my_classes)))
